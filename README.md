# Ankündigungen und Protokolle der Piraten Niederbayern

Zur Webseite: https://piraten-niederbayern.gitlab.io/

## Informationen zur Technik der Webseite

Die Seite wird mittels des Static-Site-Generator [VuePress](https://vuepress.vuejs.org/) generiert.

Jeder Push auf den `master` Branch wird von der 
[GitLab-CI](https://gitlab.com/piraten-niederbayern/piraten-niederbayern.gitlab.io/pipelines) automatisch verarbeitet. 
Siehe auch 
[`.gitlab-ci.yml`](https://gitlab.com/piraten-niederbayern/piraten-niederbayern.gitlab.io/blob/master/.gitlab-ci.yml).

## Lokale Entwicklung

Zur lokalen Entwicklung kann ein lokaler Entwicklungs-Server mit folgendem Befehl gestartet werden:

```bash
yarn dev
```
