---
prev: /protocols/
sidebar: auto
---
# Protokoll vom 03.11.2020

Ort: Online (https://meet.jit.si/PiratenstammtischPassau)  
Start: 19:15 Uhr  
Ende: 20:15 Uhr  
Teilnehmer: Jonas, Jiri, Marina, Josef

**TOP**

[[toc]]

## Attentat in Wien

Die Gruppe hat sich über die Geschehnisse in Wien ausgetauscht.

Bedauerlich ist, dass Nazis sowie rechte Parteien und Gruppierungen dies und auch die kürzlichen Vorfälle in Frankreich
zum Anlass nehmen wieder verstärkt den Blick auf sie zu lenken.
In Deggendorf findet am Donnerstagabend ein Nazi-Fackel-Marsch statt. **Marina** und **Josef** sind bei einem lokalen,
vor kurzem neu gegründeten Bündnis gegen Rechts dabei und machen bei einer Gegen-Aktion mit. Aufgrund der aktuellen
Corona-Zahlen, wird es sich dabei nicht um eine klassische Gegendemo handeln, sondern es werden stattdessen Plakate an
der Demostrecke am Stadtplatz aufgestellt.

## Online-Treffen der tschechischen Piraten

**Jiri** hat von einer Aktion der tschechischen Piraten berichtet. Dort ist die Einrichtung eines digitalen Chats ähnlich
einem "Sorgen-Telefons" geplant, an das sich Personen mit Fragen und Sorgen rund um Corona wenden können.

## Stammtische in Passau

Es wurde beschlossen, dass die kommenden Online-Stammtische erst um 20:00 Uhr beginnen.

Alle kommenden Stammtische finden bis auf Widerruf online via Jitsi (https://meet.jit.si/PiratenstammtischPassau) statt.

**Jonas** formuliert eine E-Mail mit einer Einladung zu den nächsten Stammtischen.

## Corona

**Jiri** hat berichtet, dass der Passauer Bürgermeister zu einer Online-Veranstaltung mit Tipps, wie man trotz Corona
die Freizeit gestalten kann, eingeladen hat.

Außerdem wurden beim Stammtisch folgende Fragen diskutiert: Was ist noch erlaubt, was nicht? Und sind die aktuellen
Maßnahmen zielführend. Hält sich die Bevölkerung noch daran?

Alle waren sich zudem einig, dass ein Online-Bundesparteitag eine gute Sache wäre und der physikalische Bundesparteitag
abgesagt/verschoben werden sollte.

## Termine

### Niederbayern

* **Stammtisch Dingolfing**  
  Entfällt im November
* **Wanderstammtisch Landkreis Passau dieses Mal in Titling**  
  Dienstag, 17.11.2020 20:00 Uhr, online (https://meet.jit.si/PiratenstammtischPassau)
* **Stammtisch Passau**  
  Dienstag, 01.12.2020 20:00 Uhr, online (https://meet.jit.si/PiratenstammtischPassau)
* **Stammtisch Deggendorf**  
  _Wegen Weihnachten eine Woche früher:_ Sonntag, 13.12.2020 19:00 - 21:00 Uhr,
  [Plan B Burger Society](http://planb-burger.de/)  
  alternativ online (https://meet.jit.si/PiratenstammtischDeggendorf)

### Bundesweit

* **Sondersitzung des Bundesvorstands**  
  Thema: Bundesparteitag 2020.1 und Corona  
  03.11.2020 21:00, Mumble
* **Bundesparteitag 2020.1**  
  Der BPT20.1 findet vom 14.11.2020 bis 15.11.2020 in Fürstenwalde/Spree statt.  
  Weitere Informationen unter https://wiki.piratenpartei.de/Bundesparteitag_2020.1.
* **Bundestagswahl 2021**  
  Voraussichtlich im Herbst 2021, vermutlich am 19.09.2021 oder 26.09.2021
