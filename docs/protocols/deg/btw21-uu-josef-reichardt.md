# PIRATEN zur Bundestagswahl 2021

## Bayerische Landesliste

Wir möchten zur Bundestagswahl 2021 antreten und haben dafür eine Landesliste aufgestellt.
Damit wir mit dieser auf dem Wahlzettel stehen dürfen, brauchen wir 2000 Unterstützer-Unterschriften in Bayern.

Weitere Informationen zu unserer Landesliste findest du im
[Piraten-Wiki](https://wiki.piratenpartei.de/Bundestagswahl_2021/Landesliste_BY).

## Direktkandidat für Deggendorf: Josef Reichardt

Für den Wahlkreis Deggendorf (227) haben wir **Josef Reichardt** als Wahlkreisvorschlag aufgestellt.
Damit er als Direktkandidat auf dem Wahlzettel stehen darf, benötigen wir dafür nochmal 200 Unterschriften aus dem
Wahlkreis 227.

<small>Zum Wahlkreis 227 gehören der Landkreis Deggendorf, der Landkreis Freyung-Grafenau und vom Landkreis Passau die
Gemeinden Aicha vorm Wald, Eging am See, Fürstenstein und Hofkirchen.</small>

## Unterstützen

* Wenn du aus dem Wahlkreis Deggendorf kommst, würden wir uns freuen, wenn du beide Formulare ausfüllst.
* Wenn du nicht aus dem Wahlkreis Deggendorf aber aus Bayern kommst, würden wir uns freuen, wenn du zumindest das
  Formular für die Landesliste ausfüllst.
* Wenn du nicht aus Bayern kommst, schau bitte [hier](https://wiki.piratenpartei.de/Bundestagswahl_2021#Landeslisten).

<a href="https://wiki.piratenpartei.de/wiki/images/f/f0/Unterst%C3%BCtzerUnterschriftenformular_BY.pdf" class="year-button" target="_blank">Formular für die Landesliste</a>
<a href="https://wiki.piratenpartei.de/wiki/images/a/a1/BTW21_UU_Josef_Reichardt.pdf" class="year-button" target="_blank">Formular für den Direktkandidaten</a>

Ausfüllen musst du jeweils nur diesen Bereich:

![Ausfuellanleitung](./btw21-uu-josef-reichardt_ausfuellanleitung.jpg)

Sende die unterschriebenen Formulare bitte an:

> Josef Reichardt  
> Tulpenstraße 8  
> 94469 Deggendorf

Gerne senden wir dir auch Formular per Post (natürlich inkl. vorfrankiertem Rückumschlag) zu.  
Sende dazu bitte deine Post-Anschrift und die Anzahl der gewünschten Formularen an:

> josef.reichardt@piraten-niederbayern.de

**Vielen Dank für deine Unterstützung!**
