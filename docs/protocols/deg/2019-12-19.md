---
prev: /protocols/
sidebar: auto
---
# Protokoll vom 19.12.2019

Start: 20:20 Uhr  
Ende: 21:25 Uhr  
Teilnehmer: Marina, Josef

**TOP**

[[toc]]

## Stammtische 2020

Leider hat das Café Holler im Jahr 2020 donnerstags nicht mehr geöffnet. Dementsprechend haben wir uns nun nach hin und
her Überlegungen auf den letzten paar Stammtischen dazu entschieden den Deggendorfer Stammtisch auf Sonntag zu verlegen.

Als Lokal wurde das Gasthaus Goldener Engel diskutiert. Die Entscheidung steht noch aus und sollte spätestens am ersten
Stammtisch Dingolfing am 19.01.2020 getroffen werden. Eventuell kann man sich aber auch schon vorher online einigen.

> **Anmerkung von Josef:**  
> Von Mario kam telefonisch die Idee, ob der Preysinghof Plattling vielleicht eine Alternative wäre.

Die aktuelle Planung sieht also wie folgt aus:

* Stammtisch Deggendorf
  * Organisation: **Josef**
  * Termine: Immer am dritten Sonntag jeden zweiten Monat (gerade Zahlen) von 19 - 21 Uhr
* Stammtisch Dingolfing
  * Organisation: **Mario**
  * Termine: Immer am dritten Sonntag jeden zweiten Monat (ungerade Zahlen) von 19 - 21 Uhr

Termine:

| Datum      | Uhrzeit           | Ort        |
|------------|-------------------|------------|
| 19.01.2020 | 19:00 - 21:00 Uhr | Dingolfing |
| 16.02.2020 | 19:00 - 21:00 Uhr | Deggendorf |
| 15.03.2020 | 19:00 - 21:00 Uhr | Dingolfing |
| 19.04.2020 | 19:00 - 21:00 Uhr | Deggendorf |
| 17.05.2020 | 19:00 - 21:00 Uhr | Dingolfing |
| 21.06.2020 | 19:00 - 21:00 Uhr | Deggendorf |
| 19.07.2020 | 19:00 - 21:00 Uhr | Dingolfing |
| 16.08.2020 | 19:00 - 21:00 Uhr | Deggendorf |
| 20.09.2020 | 19:00 - 21:00 Uhr | Dingolfing |
| 18.10.2020 | 19:00 - 21:00 Uhr | Deggendorf |
| 15.11.2020 | 19:00 - 21:00 Uhr | Dingolfing |
| 20.12.2020 | 19:00 - 21:00 Uhr | Deggendorf |

**Josef** möchte eine Pressemeldung für den Stammtisch Deggendorf vorbereiten. Der Text könnte dann auch als Vorlage für
Dingolfing verwendet werden.

## Piratiger Aschermittwoch 2020

Nach wie vor steht noch nicht fest, ob der piratige Aschermittwoch wieder nach Niederbayern geholt werden soll.

Der Landesvorstand bereitet aktuell die Ausschreibung vor. Gliederungen die sich im Wahlkampf befinden sollten bevorzugt
werden.

Für den Fall, dass sich keine wahlkämpfende Gliederung findet, die den PAM ausrichten möchte, erkundigt sich **Mario**
ob der Saal in Straubing, in dem der PAM18 statt fand, noch verfügbar wäre, auf welche Größe dieser teilbar wäre und wie
viel dieser kosten würde.

## Kommunalwahl 2020

In Dingolfing sind derzeit leider keine Planungen für die Kommunalwahl 2020 mehr angedacht.

In Deggendorf soll wohl von einer Gruppe, die sich für den Schutz des Klosterbergs einsetzt, noch eine Liste aufgestellt
werden. **Josef** versucht genaueres dazu in Erfahrung zu bringen.

## Termine

### Niederbayern

* 19.01.2020 19:00 Uhr: **Stammtisch Dingolfing** im China-Restaurant Sun Ly (Daimlerstr. 8)
* 16.02.2020 19:00 Uhr: **Stammtisch Deggendorf** (Das Lokal wird erst noch festgelegt, da das Café Holler leider nicht
  mehr zur Verfügung steht)

### Sonstige Termine

* 21.12.2019 15:00 Uhr: Gemeinsame **Weihnachtsfeier** :christmas_tree: der Piraten Bayern, Oberbayern und München in der LGS in München
  (Siehe auch https://forum.piratenpartei.de/t/einladung-zur-weihnachtsfeier-2019/4651)
* 06.01.2020 11:00 Uhr (Einlass) / 13:00 Uhr (Beginn): **3-Königs-Treffen** und 13. Geburtstag des Landesverbandes
  Bayern in der Sportgaststätte des FSV Erlangen (Tennenlohener Str. 68; Erlangen-Bruck)
