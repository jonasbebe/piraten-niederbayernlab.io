---
prev: /protocols/
sidebar: auto
---
# Protokoll vom 24.10.2019

Start: 20:15 Uhr  
Ende: 21:05 Uhr  
Teilnehmer: Marina, Josef

**TOP**

[[toc]]

## Kommunalwahl 2020

In Deggendorf sieht es schlecht aus für die Kommunalwahl 2020. Sowohl eine eigene Liste, als auch eine gemeinsame Liste
mit anderen Parteien scheint derzeit nicht möglich zu sein. **Josef** hat mit den Parteien _Die Linke_, _ÖDP_ und
_V-Partei³_ (alle drei derzeit nicht im Stadtrat vertreten) Kontakt aufgenommen, jedoch scheinen alle drei derzeit 
ebenfalls keine Pläne für die Kommunalwahl 2020 zu haben.

**Josef** äußerte seine Befürchtung, dass der Stadtrat weiter nach rechts rücken wird. Neben der AfD, welche sicherlich
versuchen wird einige Sitze zu ergattern, tritt auch die Wählerliste Altgemeinde Natternberg (WAN) an, welche erst 
kürzlich durch einen Neuzugang von den Republikanern "verstärkt" wurde (Quelle: 
https://www.pnp.de/lokales/landkreis_deggendorf/deggendorf/3478015_Stadtrat-Gebauer-geht-zur-WAN.html).

## Merchandising der Piraten Niederbayern

**Josef** hat nun auch noch ein Angebot für Kugelschreiber eingeholt. Dabei würde es sich um biologisch abbaubare und
nachfüllbare Stifte aus Bio-Kunststoff handeln. Er betonte jedoch auch, das Aufkleber aufgrund der Einfachheit und 
kostengünstigen Beschaffung mit höchster Priorität angegangen werden sollten.

Hier nun eine kleine Kostenübersicht (Brutto, zzgl. Versandkosten):

| Artikel                          | Preise (kleine Menge) | Preise (größere Menge)  |
|----------------------------------|-----------------------|-------------------------|
| Streichhölzer                    |   500 Stück: 335,58 € |  5.000 Stück:  924,63 € |
| Bleistifte                       | 1.008 Stück: 407,27 € | 14.400 Stück: 4355,40 € |
| Kugelschreiber                   | 1.000 Stück: 652,12 € | 10.000 Stück: 4162,62 € |
| Notizblöcke (DIN A5)             |   250 Stück: 204,98 € |  1.000 Stück:  676,59 € |
| Einkaufswagenchips (Bio-Plastik) | 1.000 Stück: 351,05 € | 10.000 Stück: 1041,25 € |
| Aufkleber (z. B. 5 x 5 cm)       |   100 Stück:  31,73 € |  1.000 Stück:   49,20 € |

## Support von Fridays for Future

Derzeit wird geplant ein öffentliches Treffen zu organisieren, in dem Forderungen an die Kommunalpolitiker (Stadt & 
Landkreis) erarbeitet werden sollen. **Marina & Josef** waren dazu auch bereits auf einem ersten Orga-Treffen. Dort weiß
zwar jeder über die Parteimitgliedschaft Bescheid, offiziell ist jedoch keine Partei-Beteiligung erwünscht, weshalb die
Piratenpartei in diesem Zusammenhang leider nicht öffentlich genannt wird.
Das Treffen soll entweder am 7. oder 14.11. stattfinden. Derzeit wird noch nach einer passenden Location gesucht.
Weitere Details werden bei Zeiten bekannt gegeben.

Am 29.11. findet in Deggendorf wieder ein Demozug statt. Die Anmeldung und die Rolle des Versammlungsleiters übernimmt
wieder **Josef**, **Marina** ist dieses mal stellvertretende Versammlungsleiterin.

Treffpunkt ist 12:15 Uhr, um 12:30 Uhr beginnt der Demozug. Bis 13:30 Uhr sollte der Demozug zu Ende sein. Direkt im
Anschluss beginnt am _Bunten Markt_ (direkt neben dem Ziel des Demozugs) die Info-Veranstaltung _Voices for Future_.

## Aktueller Stand BzV Niederbayern/Oberpfalz

Zum gemeinsamen Bezirksverband Niederbayern Oberpfalz wird es auf dem Bezirksparteitag Oberpfalz einen Diskussionsslot
geben. **Marina & Josef** planen dort als Gäste hinzufahren.

## Stammtische 2020

Auf dem letzten Port Niederbayern wurde eine Neuregelung der Stammtische besprochen.

Bzgl. Deggendorf wurde angeregt, diese auf einen anderen Tag (sonntags) und in ein anderes Lokal (Goldener Engel, das 
Café Holler hat sonntags geschlossen) zu verlegen. **Marina & Josef** sind, nachdem sie sich darüber Gedanken gemacht
haben, zu dem Entschluss gekommen, dass es sehr schade wäre das Café Holler zu verlassen. Möglicherweise war die
Entscheidung etwas übereilt. Schließlich haben sich hier immer wieder Leute (_"Laufkundschaft"_) gefunden, die sich an
den Stammtischen beteiligt haben. Außerdem ist das Lokal uns gegenüber sehr aufgeschlossen und bekundet immer wieder
wie toll sie finden, dass wir da sind.

Probleme, die zum Andenken der Verlegung geführt haben, waren:
1. Lautstärke im Café Holler (teilweise Livemusik)
2. Wochentag

Den ersten Punkt könnte man lösen, in dem man den Stammtisch wieder auf 19 Uhr vorverlegt. Wir hatten uns im
vergangen Jahr aufgrund von 2 - 3 Ausnahmen, wo wir erst um 20 Uhr starten hätten können, darauf geeinigt einheitlich
alle Stammtische auf 20 Uhr zu setzen. Das war möglicherweise ein Fehler.

Der zweite Punkt ist eine unendliche Geschichte. Verschiedene Gespräche haben nun wieder gezeigt, dass Donnerstag
vielleicht doch beliebter sein könnte als Sonntag. _"Wie man's macht, macht man's verkehrt..."_ :wink:

Vorschlag von **Marina & Josef**, der auf dem nächsten Port Niederbayern (am 11.11.2019 um 19 Uhr im Goldenen Schiff in
Passau) diskutiert werden sollte:
* Den Stammtisch Deggendorf doch auf donnerstags und im Café Holler lassen.  
  (Bisher wurde die Termin-Änderung noch nicht kommuniziert, wäre also noch kein Problem)
* Evtl. den neuen Stammtisch Dingolfing ebenfalls auf donnerstags legen.  
  (Sollte es bei Sonntag bleiben, wäre der erste Termin am 19.01.2020, wenn er auch auf Donnerstag gelegt wird schon am 
  16.01.2020. **Mario** sucht derzeit noch ein Lokal, sollte _"das perfekte Lokal"_ Donnerstags geschlossen haben, 
  sollte als Termin Sonntag verwendet werden und umgekehrt.)

## Termine

### Niederbayern

* 11.11.2019 19:00 Uhr: **Port Niederbayern** im November
  ([Facebook-Veranstaltung](https://www.facebook.com/events/551187515292969/))
  im Goldenen Schiff in Passau
* 19.12.2019 20:00 Uhr: **Stammtisch Deggendorf** im Dezember
  ([Facebook-Veranstaltung](https://www.facebook.com/events/978827982241383/))
  im _Café Holler_
* 29.11.2019 12:15 Uhr: **#NeustartKlima** - Demonstration von **Fridays for Future Deggendorf**
  (Start und Ziel ist am _Luitpoldplatz_)  
  Direkt im Anschluss (ca. 13:30 Uhr): _Voices for Future_ am _Bunten Markt_ (_Luitpoldplatz_)

### Sonstige Termine

* 09.11.2019 - 10.11.2019: **Bundesparteitag 2019.2** in Bad Homburg v. d. Höhe  
  Unter anderem mit der Neuwahl unseres Bundesvorstandes  
  Weitere Informationen unter https://wiki.piratenpartei.de/Bundesparteitag_2019.2
* 16.11.2019 12:30 Uhr: **Bezirksparteitag Oberpfalz** im _RESI_ in Regensburg  
  Unter anderem mit der Diskussion zum gemeinsamen Bezirksverband Niederbayern Oberpfalz
