# Protokoll vom 20.09.2020

Ort: Sun Ly, Dingolfing  
Start: 19:00 Uhr  
Ende: 21:30 Uhr  
Teilnehmer: Jonas, Mario, Marina, Josef, Tobias

**TOP**

[[toc]]

## Globaler Klimastreik - Fridays for Future - Pirates for Future

Am 25.09.2020 findet wieder ein globaler Klimastreik von Fridays for Future statt. Bisher bekannte Demos in Niederbayern
finden in Deggendorf, Dingolfing, Landshut, Passau, Straubing und Vilsbiburg statt.

**Josef** ist in Deggendorf wieder Versammlungsleiter und tritt diesmal auch zum ersten Mal als Mitglied der
Piratenpartei auf. Bei den vorherigen Terminen war dies von FFF Deggendorf ausdrücklich nicht gewünscht.

## Bundestagswahl 2021

### Aufstellungsversammlungen für Kreiswahlvorschläge

**Josef** möchte sich als Kreiswahlvorschlag aufstellen lassen. Für die Planung einer AV hat er durch den LaVo eine E-Mail
an alle Mitglieder des Wahlkreises verschicken lassen:
https://cryptpad.piratenpartei.de/code/#/2/code/view/ZQe3cKvsf8-5KKxpeIxe75KmLTrxtGcaLEtzTAindkE/

**Mario** überlegt für den Wahlkreis 230 Rottal-Inn (Landkreis Rottal-Inn, Landkreis Dingolfing und aus dem Landkreis
Landshut die Gemeinden Aham, Gerzen, Kröning, Postau, Schalkham, Weng und Wörth an der Isar) das Gleiche zu tun.

Weitere AVs sind in Niederbayern nach aktuellem Kenntnisstand nicht geplant.

### Unterstützerunterschriften

In den Wahlkreisen wo noch AVs für Kreiswahlvorschläge ausstehen werden aktuell noch keine Unterschriften für die
Landesliste gesammelt. Ansonsten müsste man danach die gleichen Leute ein zweites Mal um eine Unterschrift für den
Wahlkreis bitten, dies soll verhindert werden.

**Josef** hat eine E-Mail an den LaVo geschrieben, mit der Frage, ob es Planungen gibt bzgl. Merchandising o. Ä. gibt,
welches zum Unterschriften sammeln verwendet werden kann. Die SG BTW21 wird ja erst zum eigentlichen Wahlkampf Material
liefern können.

### SGs

**Jonas**, **Josef** und **Marina** haben sich bei der letzten Sitzung der SG BTW21 bzw. SG Design eingebracht.

Neue Termine der beiden SGs werden erst bei Bedarf wieder festgelegt.

## Umwelt-Flyer

**Marina** hat den Umwelt-Flyer finalisiert und **Josef** hat diesen in Druck gegeben. Für Niederbayern stehen somit
demnächst 500 Stück zur Verfügung.

## Arbeitsweise BuVo & Bundesparteitag 2020.1

Tobias hat über die Arbeitsweise im BuVo berichtet. Außerdem wurde ausgiebig über den BPT20.1 und die dazu gestellten
Anträge diskutiert.

## Stammtisch Deggendorf

Da das Gasthaus Goldener Engel künftig keine Parteiwerbung in Form unseres Tischfähnchens mehr haben möchte, mussten wir
ein neues Lokal suchen. Da der Stammtisch ja auch Anlaufstelle für Interessierte, Gäste, Neupiraten sein soll, ist es
keine Option auf dieses Fähnchen zu verzichten.

**Mario** hat glücklicherweise Beziehungen zum "Plan B" (Burger-Lokal) und hat dort einen Tisch für uns reserviert.
Das Fähnchen dürfen wir dort aufstellen und es gibt auch tolles veganes Essen. :+1:

## Telegram-Kanal Piraten Niederbayern

**Josef** legt einen Telegram-Kanal für die Piratenpartei Niederbayern an. Das soll kein Gruppenchat, sondern ein 
"Kanal" sein, über den dieselben Informationen wie auch auf Facebook, Instagram und Twitter geteilt werden.

> **Anmerkung von Josef:**  
> Ich habe den Kanal soeben angelegt. Marina und ich sind Administratoren und kümmern uns darum, solange niemand etwas
> dagegen hat.
>
> Den Kanal findet ihr hier: https://t.me/piraten_ndb

## Termine

### Niederbayern

* **Stammtisch Passau**  
  Dienstag, 06.10.2020 19:00 Uhr, [Akropolis Athen](https://www.akropolis-athen.com/)
* **Stammtisch Deggendorf**  
  Sonntag, 18.10.2020 19:00 Uhr, [Plan B Burger Society](http://planb-burger.de/)
* **Wanderstammtisch Landkreis Passau dieses Mal in Eging am See**  
  Dienstag, 20.10.2020 19:00 Uhr, [Urweisse Hütt’n](https://urweisse-huette.de/)
* **Stammtisch Dingolfing**  
  _Aufgrund eines Terminkonfliktes mit BPT20.1 muss der Termin im November entfallen oder verschoben werden -
  Entscheidung steht noch aus._

### Bundesweit

* **Bundesparteitag 2020.1**  
  Der BPT20.1 findet vom 14.11.2020 bis 15.11.2020 in Fürstenwalde/Spree statt. Weiter Informationen unter
  https://wiki.piratenpartei.de/Bundesparteitag_2020.1.
* **Bundestagswahl 2021**  
  voraussichtlich im Herbst 2021, vermutlich am 19.09.2021 oder 26.09.2021
