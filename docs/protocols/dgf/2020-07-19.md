# Protokoll vom 19.07.2020

Ort: Sun Ly, Dingolfing  
Start: 19:00 Uhr  
Ende: 21:30 Uhr  
Teilnehmer: Jonas, Mario, Marina, Josef

> **Anmerkung von Josef:**  
> Da dies der erste Stammtisch nach einer längeren Corona-Pause war, hatten wir keine konkrete Agenda, sondern haben uns
> einfach von der Diskussion treiben lassen. Entsprechend fällt das Protokoll dieses Mal nicht so detailliert aus.

**TOP**

[[toc]]

## Corona-Warn-App

Es wurde kontrovers über den tatsächlichen Nutzen der App und dessen Sicherheits- und Datenschutzkonzept diskutiert.

Josef brachte den Einwand, dass man zu großen Teilen der App gar keine Aussage treffen könne, weil wesentliche Teile
hinter der Google/Apple API "PPCP" versteckt sind, deren Quelltext nicht frei zugänglich ist. Somit ist die Aussage der
Regierung, dass die App Open Source sei auch nur bedingt korrekt und unter Beachtung der genannten Tatsache irrelevant.

Dennoch wird die App von der Mehrheit der Anwesenden nach aktuellem Kenntnisstand befürwortet.

> **Anmerkung von Josef:**  
> Weitere Informationen sind im Bug-Tracker der offiziellen Corona-Warn-App zu finden:
> https://github.com/corona-warn-app/cwa-app-android/issues/75

## Bundestagswahl 2021

Eine kurze Diskussion über die Bundestagswahl im nächsten Jahr hat ergeben, dass man in Niederbayern durchaus motiviert
ist, bei der Sammlung der Unterstützer-Unterschriften aktiv zu werden. Konkrete Pläne wurden jedoch noch nicht gemacht.
Allerdings wurde die Möglichkeit angesprochen hier mit der Oberpfalz zusammenzuarbeiten, falls dort Interesse besteht.

Außerdem gibt es vereinzelt durchaus auch Überlegungen, sich für die Landeliste aufzustellen. Diese muss schließlich
aufgestellt werden, bevor man mit dem Unterschriftensammeln anfangen kann.

> **Anmerkung von Josef:**  
> Die Aufstellungsversammlung findet einen Tag nach dem Landesparteitag, also am 06.09.2020 ab 10:00 Uhr
> (Akkreditierung: 09:00 Uhr) in Lauf an der Pegnitz (Mittelfranken) statt.
>
> Weitere Informationen zum Landesparteitag 2020.1 sind im Piraten-Wiki zu finden:
> [https://wiki.piratenpartei.de/BY:Landesparteitag_20.1#Information_zur_AV-BTW21:](https://wiki.piratenpartei.de/BY:Landesparteitag_20.1#Information_zur_AV-BTW21:)
> bzw. https://wiki.piratenpartei.de/BY:Landesparteitag_20.1/AV-Kandidaten

## Landesparteitag 2020.1

Natürlich kam auch der anstehende Landesparteitag kurz zur Sprache. Konkrete Pläne wer hinfährt, wurden aber noch nicht
diskutiert.

> **Anmerkung von Josef:**  
> Der Landesparteitag 2020.1 findet am Freitag, den 04.09.2020 ab 17:00 Uhr (Akkreditierung: 16:00 Uhr) und am Samstag,
> den 05.09.2020 ab 10:00 Uhr (Akkreditierung: 09:00 Uhr) in Lauf an der Pegnitz (Mittelfranken) statt.
>
> Weitere Informationen zum Landesparteitag 2020.1 sind im Piraten-Wiki zu finden:
> https://wiki.piratenpartei.de/BY:Landesparteitag_20.1

## Patrick Breyer in der Tagesschau

Vergangene Woche war Patrick Breyer in der Tagesschau zu sehen. Erfreulich war dabei auch, dass er relativ lange zu Wort
kam und mit Name und "Piratenpartei" eingeblendet wurde.

Der Artikel zum Beitrag ist hier zu finden: https://www.tagesschau.de/inland/verfassungsgericht-datenauskunft-101.html

## Umweltflyer

Josef und Marina hatten Ende letzten Jahres einen Flyer in Zusammenarbeit mit der AG Umwelt auf bundesebene erstellt.
Dieser wurde dann aber leider bis heute nicht gedruckt, da auf bundesebene derzeit an einem neuen allgemeinen
Flyer-Design gearbeitet wird.

**Marina** wird nun den Flyer entsprechend dem aktuellen (leider noch nicht finalen) Stand der Design-Vorlage anpassen
und im Anschluss wird dieser in Druck gehen, darum kümmert sich dann **Josef**. Für Niederbayern werden 500 Flyer
bereitgestellt.

## Gemeinsamer Bezirksverband mit der Oberpfalz

Vor allem in Bezug auf die Bundestagswahl im kommenden Jahr wäre eine Bündelung der Ressourcen von Vorteil. Dazu wäre
es sinnvoll die Gründung eines gemeinsamen Bezirksverbandes Niederbayern-Oberpfalz noch in diesem Jahr voranzutreiben.

**Josef** wird versuchen hier demnächst ein entsprechendes Vorbereitungstreffen zu initiieren. Es ist noch zu klären, ob
dieses physikalisch oder virtuell stattfindet.

## Termine

### Niederbayern

* **Stammtisch Deggendorf**  
  Sonntag, 16.08.2020 19:00 - 21:00 Uhr, Gasthaus Goldener Engel  
  (**Josef & Marina** kümmern sich um die Reservierung)
* **Stammtisch Dingolfing**  
  Sonntag, 20.09.2020 19:00 - 21:00 Uhr, Sun Ly

### Bayern

* **Landesparteitag 2020.1**  
  Wollnersaal, Neunkirchener Str. 6, 91207 Lauf an der Pegnitz  
  Freitag 04.09.2020 ab 17:00 Uhr (Akkreditierung 16.00 Uhr)  
  Samstag 05.09.2020 ab 10:00 Uhr (Akkreditierung 09.00 Uhr)  
* **Aufstellungsversammlung für die Landesliste zur Bundestagswahl 2021**  
  Wollnersaal, Neunkirchener Str. 6, 91207 Lauf an der Pegnitz  
  Sonntag 06.09.2020 ab 10:00 Uhr (Akkreditierung 09.00 Uhr)

### Bundesweit

* **Bundestagswahl 2021**  
  voraussichtlich im Herbst 2021, vermutlich am 19.09.2021 oder 26.09.2021
* **Bundesparteitag 2020.1**  
  Der Bundesparteitag in Bad Homburg wurde abgesagt. Ein neuer Termin steht noch nicht fest.
