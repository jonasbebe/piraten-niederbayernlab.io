---
prev: /protocols/
sidebar: auto
---
# Protokoll vom 11.11.2019

Start: 20:20 Uhr  
Ende: 22:00 Uhr  
Teilnehmer: Mario, Marina, Josef

**TOP**

[[toc]]

## Kommunalwahl 2020

Weiterhin bestehen weder in Dingolfing noch in Deggendorf konkrete Pläne für die Kommunalwahl 2020. Zum Teil wurden
bereits Gespräche mit anderen Parteien geführt, manche stehen aber noch aus.

Leider sieht es aber derzeit eher schlecht aus.

## Merchandising der Piraten Niederbayern

Wir warten hier zum einen darauf, dass **Marina** Designvorschläge macht, zum anderen aber auch noch auf einen
konkreteren Plan, wie es mit dem BzV Oberpfalz weitergeht. Sobald man genaueres fest steht, kann man ggf. gemeinsam 
über das weitere Vorgehen beraten.

Möglichst bald soll es jedoch wieder Termin-Kärtchen für die niederbayerischen Stammtische in Deggendorf und Dingolfing
geben.

## Stammtische 2020

### Deggendorf

Der Stammtisch Deggendorf bleibt nun doch im Café Holler und findet dort immer am dritten Donnerstag jeden zweiten 
Monat (gerade Zahlen) von 19 - 21 Uhr statt.

Konkret wären das also folgende Termine:

* ~~20.02.2020 _Unsinniger Donnerstag_, deshalb sollten wir diesen Termin um eine Woche vorverlegen auf den 13.02.2020~~
* ~~16.04.2020~~
* ~~19.06.2020~~
* ~~20.08.2020~~
* ~~15.10.2020~~
* ~~17.12.2020~~

> **Anmerkung von Josef:**  
> Kurz nach dem Stammtisch erfuhren wir, dass das Café Holler seine Öffnungszeiten ändert und leider im Jahr 2020
> donnerstags nicht mehr geöffnet hat.
> Siehe [Stammtisch Deggendorf am 19.12.2019](../deg/2019-12-19.md#stammtische-2020).

### Dingolfing

Hier bleibt alles wie im September besprochen:

Ort: **Mario** liefert einen Vorschlag.

Termine: Immer am dritten Sonntag jeden zweiten Monat (ungerade Zahlen) von 19 - 21 Uhr

* 19.01.2020
* 15.03.2020
* 17.05.2020
* 19.07.2020
* 20.09.2020
* 15.11.2020

### Zusätzliche Stammtische

Da es 2020 keinen wandernden Stammtisch "Port Niederbayern" mehr gibt, haben wir überlegt ob wir unter diesem Namen
stattdessen zusätzlich zwei mal im Jahr einen Stammtisch in Plattling abhalten und einmal eine Musik-Veranstaltung im
Café Holler.

Konkrete Pläne wurden jedoch noch nicht ausgearbeitet.

## Bundesparteitag 2019.2

Am 9. und 10. November fand der zweite Bundesparteitag 2019 der Piratenpartei Deutschland in Bad Homburg v.d. Höhe
statt. Unter anderem wurde der Bundesvorstand neu gewählt und der Klimanotstand ausgerufen. Alle weiteren Informationen
sind in unserem Wiki zu finden: https://wiki.piratenpartei.de/Bundesparteitag_2019.2

## Piratiger Aschermittwoch 2020

Nach wie vor steht noch nicht fest, ob der piratige Aschermittwoch wieder nach Niederbayern geholt werden soll.
**Mario** erkundigt sich schon mal nach möglichen Veranstaltungsorten und den damit verbundenen Kosten.

## Termine

### Niederbayern

* 29.11.2019: **#NeustartKlima**  
  z. B. in Deggendorf:
  * Treffpunkt um 12:15 Uhr am Luitpoldplatz neben der Grabkirche
  * Direkt im Anschluss um 13:30 Uhr _Voices for Future_ am _Bunten Markt_
* 19.12.2019 20:00 Uhr: **Stammtisch Deggendorf** im Dezember
  ([Facebook-Veranstaltung](https://www.facebook.com/events/978827982241383/))
  ~~wie immer im _Café Holler_~~ im Goldenen Engel

### Sonstige Termine

- 16.11.2019 12:30 Uhr: **Bezirksparteitag Oberpfalz 2019.1** in Regensburg im RESI
  (https://forum.piratenpartei.de/t/bezirksparteitag-oberpfalz-19-1/4546)
- **Landesparteitag Bayern 2020.1**: Der nächste Landesparteitag Bayern findet erst nach der Kommunalwahl (15.03.2020)
  und somit voraussichtlich am 25./26.04.2020 statt.
