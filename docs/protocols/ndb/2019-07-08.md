---
prev: /protocols/
sidebar: auto
---
# Protokoll vom 08.07.2019

Start: 19:30 Uhr  
Ende: 21:00 Uhr  
Teilnehmer: Marina, Josef

**TOP**

[[toc]]

## Kurzbericht zur AG Umwelt

Am Mittwoch, den 03.07.2019 fand ein "Reaktivierungs-Mumble" der AG Umwelt statt. **Marina** und **Josef** haben daran 
teilgenommen und kurz berichtet:

In erster Linie ging es um's Kennenlernen derer, die das Treffen initiiert haben und wo es mit der AG Umwelt, die zuletzt 
nicht mehr aktiv war, hingehen könnte. Ein konkretes weiteres Vorgehen wurde jedoch noch nicht geplant.

Der nächste Mumble-Termin findet am Mittwoch den 10.07.2019 ab 20:00 Uhr auf dem NRW-Server im Raum der AG Umwelt statt.
Unter anderem ist für dieses Treffen die Koordinatorenwahl geplant.

**Marina** & **Josef** planen, wieder daran teilzunehmen.

## Merchandising der Piraten Niederbayern

Da uns langsam das Werbematerial ausgeht, haben wir schon öfter darüber gesprochen, dass wir neues Merchandising
benötigen. Mit den Produkten, die im P-Shop verfügbar sind, konnten wir uns nicht wirklich anfreunden, deshalb haben wir
vor einiger Zeit schon vereinbart, dass **Marina** und **Josef** recherchieren, welche Möglichkeiten wir hier haben.
Aufgrund der Europawahl hat sich das Thema leider immer wieder verzögert aber nun konnten wir eine Liste an Möglichen
Produkten vorlegen:

- Streichhölzer
- Bleistifte
- Notizblöcke (DIN A5)
- Einkaufswagenchips

Wenn wir allerdings nur für Niederbayern relativ geringe Stückzahlen bedrucken lassen, wird es entsprechend teuer. Zu
jedem Produkt wurden Beispiel-Rechnungen angefertigt, wie sich die Preise verhalten, wenn wir je zwei Gliederungen
finden, die das Produkt in der selben Stückzahl bestellen würden oder wenn wir eine Einrichtung (z. B. P-Shop, größere 
Gliederung, ...). Dabei wird schnell klar wie immens das Einsparpotential ist, wenn man große Stückzahlen
(Größenordnung: 10 Gliederungen mit dem selben Bedarf wie Niederbayern).

Prinzipiell wären zwei Herangehensweisen denkbar:
1. Anfrage an P-Shop
2. Selbst nach anderen Gliederungen (z. B. via Discourse) suchen und Sammelbestellungen organisieren

Derzeit läuft übrigens eine Diskussion ob der P-Shop noch sinnvoll ist:
https://forum.piratenpartei.de/t/benoetigt-die-pp-einen-eigenen-pshop/3466  
Solange diese Diskussion noch läuft, sollten wir zumindest noch bis zum nächsten Treffen warten und dann entscheiden
wie wir hier weiter verfahren wollen.

Bis dahin versuchen **Marina** und **Josef** Ideen bzw. Design-Vorschläge zu erarbeiten.

Hier nun die Preis-Berechnungen und weitere Informationen zu den Produkten:  
<a href="/assets/2019-07-08/Merchandising-NDB.pdf" target="blank" style="text-decoration: none;">:page_facing_up: Merchandising-NDB.pdf</a>

Zur Überbrückung schaut **Josef** nach der Sommerpause des P-Shops welche Sticker es dort gibt, damit wir beim nächsten
Treffen entscheiden können, ob wir unser restliches Budget für Niederbayern-Werbemittel zumindest teilweise dafür
ausgeben wollen.

Sollte jemand Vorschläge haben, gerne bei **Josef** melden.

> **Anmerkung von Josef:**  
> Soweit ich weiß sind noch 200,81 € von dem Budget für Niederbayern-Werbemittel übrig.
>
> Das Aufkleber-Angebot im P-Shop ist m.M.n. nicht besonders toll. Weiteres Vorgehen wird nochmal überdacht...

## Gebietsversammlung Niederbayern

Zur Gebietsversammlung gibt es eine Diskussion in Discourse, in der es darum geht, ob diese während oder nach den 
Sommerferien abgehalten werden soll.

Für den Veranstaltungsort ist beim LaVo bisher lediglich ein einziger Vorschlag (der von **Josef**: Hotel Liebl in 
Plattling) eingegangen.

> **Anmerkung von Josef:**  
> Das Hotel Liebl in Plattling steht nun leider doch nicht mehr zur Verfügung. Es wird aktuell nach Alternativen
> gesucht.

Es wird außerdem diskutiert, ob das Abhalten dieser Gebietsversammlung in der LGS in München, und somit außerhalb des 
betroffenen Gebiets, möglich wäre und ob es sich tatsächlich um eine Gebietsversammlung handelt, bei der auch die
entsprechende Ladungsfrist eingehalten werden muss.

**Josef** äußerte seine Meinung, dass unabhängig davon, ob laut Satzung eine Gebietsversammlung mit entsprechender 
Ladungsfrist für die Gründung einer Gliederung nötig sei oder nicht, es auf jeden Fall Sinn mache alle Mitglieder mit 
einer entsprechenden Vorlaufzeit einzuladen. Des Weiteren wäre eine Gebietsversammlung außerhalb Niederbayerns 
abzulehnen, da es überflüssig wäre über eine eigene Gliederung zu diskutieren, wenn wir nicht einmal das Organisieren 
einer Räumlichkeit für die entsprechende Gründungsversammlung schaffen würden.

Hier geht's zur Diskussion auf Discourse:
https://discourse.piratenpartei-bayern.de/t/termin-mitglieder-gebiets-versammlung-niederbayern/1954

## Piraten Niederbayern im Internet

Aktuell schreibt **Josef** nach einem Port Niederbayern oder Stammtisch Deggendorf immer ein Protokoll, veröffentlicht
dieses unter https://piraten-niederbayern.gitlab.io/ und schreibt anschließend eine Nachricht in unserer internen
"Piraten Niederbayern"-Twitter-Gruppe (derzeitige Mitglieder: **Mario**, **Marina** & **Josef**).

Gäste haben so nicht die Möglichkeit darüber informiert zu werden, wann das Protokoll verfügbar ist.
Auch wenn Discourse sicherlich nicht bei jedem Gast (oder auch Mitglied) bekannt ist, wird **Josef** künftig den Link
aufs Protokoll immer unter die jeweilige Termin-Einladung im Discourse schreiben.

Kurzzeitig wurde überlegt eine Liste bei den Treffen auszulegen, wo man seine E-Mail-Adresse eintragen kann, wenn man 
Infos möchte. Diese Idee wurde aber zum einen aus Datenschutzgründen und zum anderen deshalb, weil wir keine Neuauflage 
der Mailing-Listen einführen wollen, wieder verworfen.  
Unser aktuelles Kommunikationsmedium ist Discourse.

Außerdem wird **Josef** beim LaVo nachfragen, ob die Möglichkeit besteht die Domain `piraten-niederbayern.de` (welche 
derzeit eine Weiterleitung auf `piratenpartei-bayern.de` ist) für diese Webseite zu benutzen.

## Sonstige Themen

### Aktive AGs in Bayern

Es wurde darüber gesprochen, das es aktuell schwer ist zu erfahren, welche AGs es gibt und was diese machen.

**Marina** versucht via Discourse herauszufinden, ob es derzeit überhaupt noch aktive AGs in Bayern gibt, welche das 
sind und was die tun, bzw. insbesondere wann sich diese wo treffen.

### Mehr Termine sammeln

Künftig wollen wir versuchen alle Termin, die lokal oder überregional relevant sind, sammeln und zu unseren Treffen 
ankündigen, um mehr Möglichkeiten zur Vernetzung zu schaffen.

Zu den relevanten Terminen gehören Mumbles, größere Demos oder sonstige größere Events in ganz Bayern aber keine 
Stammtische oder ähnliche kleinere Veranstaltungen.

## Nächste Termine

### Niederbayern

- 22.08.2019 ab 20:00 Uhr: Stammtisch Deggendorf im Café Holler
  ([Veranstaltung auf Facebook](https://www.facebook.com/events/532593317237272/))
- 09.09.2019 ab 19:00 Uhr: Port Niederbayern in Straubing im Café Lebensgefühl
  ([Veranstaltung auf Facebook](https://www.facebook.com/events/913003538909240/))

#### Gebietsversammlung in Planung

Einladung mit weiteren Details erfolgt durch den LaVo.  
Termin voraussichtlich August bis September, Veranstaltungsort evtl. in Plattling.

Zur Diskussion:
https://discourse.piratenpartei-bayern.de/t/termin-mitglieder-gebiets-versammlung-niederbayern/1954

### Weitere Termine

- Christopher-Street-Day 2019 in München
  ([Veranstaltung auf Facebook](https://www.facebook.com/events/2391446591125409/))

### AGs

- 10.07.2019 ab 20:00 Uhr: Mumble der AG Umwelt (NRW-Server; Raum "AG Umwelt")
