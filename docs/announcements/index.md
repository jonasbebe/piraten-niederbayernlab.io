---
prev: /
next: /protocols/
---
# Ankündigungen

Derzeit gibt es in Niederbayern verschiedene Anlaufstellen, wenn Ihr mit Piraten in Kontakt treten wollt.

Grundsätzlich gilt: **Jeder ist willkommen.**  
Wir freuen uns immer über neue Gesichter, egal ob Mitglieder der Piratenpartei, Interessierte oder Neugierige. 

Die konkreten Einladungen zu den nächsten Terminen findet ihr im
[Forum](https://forum.piratenpartei.de/tags/niederbayern) oder auf unseren Kanälen in den sozialen Netzen:

<div style="text-align: center; margin-top: -0.5em;">
    <a href="https://www.facebook.com/PiratenparteiNiederbayern" title="Facebook">
        <img src="/img/social/facebook.svg" alt="facebook" style="width: 36px; margin-right: 4px;" />
    </a>
    <a href="https://www.instagram.com/piratenparteiniederbayern" title="Instagram">
        <img src="/img/social/instagram.svg" alt="instagram" style="width: 36px; margin-right: 4px;" />
    </a>
    <a href="https://twitter.com/piraten_ndb" title="Twitter">
        <img src="/img/social/twitter.svg" alt="twitter" style="width: 36px; margin-right: 4px;" />
    </a>
    <a href="https://t.me/piraten_ndb" title="Telegram">
        <img src="/img/social/telegram.svg" alt="telegram" style="width: 36px; margin-right: 4px;" />
    </a>
</div>

<br>Hier die niederbayerischen Piraten-Stammtische in alphabetischer Reihenfolge:

[[toc]]

## Stammtisch Deggendorf

Der Deggendorfer Stammtisch findet immer am dritten Sonntag jeden zweiten Monat (gerade Zahlen: Februar, April, ...) ab
19 Uhr im [Plan B Burger Society](http://planb-burger.de/) statt.

## Stammtisch Dingolfing

Der Stammtisch Dingolfing findet in der Regel immer am dritten Sonntag jeden zweiten Monat (ungerade Zahlen: Januar,
März, ...) ab 19 Uhr im Sun Ly ([Facebook-Seite](https://www.facebook.com/restaurant.sunly/)) statt.

## Stammtisch Passau

Der Passauer Stammtisch findet immer am ersten Dienstag im Monat ab 19 Uhr im
[Akropolis Athen](https://www.akropolis-athen.com/) statt.

## Wanderstammtisch Landkreis Passau

Im Landkreis Passau findet immer am dritten Dienstag im Monat ein "Wanderstammtsich" statt. Dabei werden verschiedene
Orte im Landkreis Passau besucht. Details werden über die oben genannten Kanäle bekannt gegeben.
