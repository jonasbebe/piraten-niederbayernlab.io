module.exports = {
  title: 'Piraten Niederbayern',
  description: 'Ankündigungen & Protokolle',
  locales: {
    '/': {
      lang: 'de-DE',
    },
  },
  plugins: [
    ['@vuepress/last-updated', {
      transformer: (timestamp) => {
        const date = new Date(timestamp);
        const d = `0${date.getDate()}`.substr(-2);
        const m = `0${date.getMonth()}`.substr(-2);
        const y = date.getFullYear();
        return `${d}.${m}.${y}`;
      },
    }],
    '@vuepress/medium-zoom',
    require('./components/footer/plugin.js'),
  ],
  themeConfig: {
    docsDir: 'docs',
    nav: [
      {
        text: 'Startseite',
        link: '/'
      },
      {
        text: 'Ankündigungen',
        link: '/announcements/'
      },
      {
        text: 'Protokolle',
        link: '/protocols/'
      },
    ],
    lastUpdated: 'Letzte Aktualisierung',
    repo: 'https://gitlab.com/piraten-niederbayern/piraten-niederbayern.gitlab.io',
    editLinks: true,
    editLinkText: 'Seite bearbeiten',
  },
  dest: 'public',
  extendMarkdown: md => {
    md.set({ linkify: true });
  }
};
